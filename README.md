# README

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto HEATMAP.

### O QUE FAZER ?

- Você deve fazer um fork deste repositório, criar o código e ao finalizar realizar o commit e solicitar um pull request, nós iremos avaliar e retornar por email o resultado do seu teste.

### ESCOPO DO PROJETO

Objetivo é replicar o quadro de atividades do github(imagem abaixo) dado o json ([data.json](data.json)) com as leituras.
Exiba os resultados por ano e coloque um seletor de ano. Pode testar em qualquer lib, mas use React como framework.

![Getting Started](./sample.jpeg)

### Critérios de avaliação

Entrega no prazo

- Simplicidade da solução
- Complexidade algorítmica
- Boas práticas de desenvolvimento de software
- Solução feita utilizando React
- Diferenciais: utilização de conceitos de programação funcional.

### Informações Importantes

NÃO USAR nenhum plugin pronto (ex.: http://patientslikeme.github.io/react-calendar-heatmap/)
Levar em consideração que o json pode receber novos dados
Colocar um tooltip em cada quadradinho verde, informando a quantidade.
É importante o seletor de ano, a organização de mês e principalmente a de semanas.
